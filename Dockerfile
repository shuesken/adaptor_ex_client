# build
FROM node:current-alpine AS builder
# Create app directory
WORKDIR /usr/src/app
COPY package*.json ./
RUN npm install
COPY . .
# workaround for https://github.com/webpack/webpack/issues/14532
ENV NODE_OPTIONS=--openssl-legacy-provider
RUN npm run build

FROM joseluisq/static-web-server:latest
COPY --from=busybox:1.36.1-uclibc /bin/sh /bin/sh
COPY --from=busybox:1.36.1-uclibc /bin/ls /bin/ls
COPY --from=busybox:1.36.1-uclibc /bin/vi /bin/vi
# set fallback because we use client side router
ENV SERVER_FALLBACK_PAGE /public/index.html
COPY --from=builder /usr/src/app/dist/ /public/
EXPOSE 80/tcp

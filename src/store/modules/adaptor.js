import { setAuthToken, removeCrossSessionItem, getCrossSessionItem, isUrlSafe, i18n, hasKey } from "@utils";
import { useToast } from "vue-toastification";

const toast = useToast();

const state = {
  view: "Login", // current View
  games: [], // info about all games on this adaptor server
  authorized: true, // is there an auth token
  clientLog: [], // list of errors and events
  zoom: 1,
  username: "me",
  others: [],
  prompt: {
    visible: false,
    message: null,
    webhook: null,
    schema: null,
  },
};

const getters = {
  cssClassesByPlugin: () => (plugin) => {
    const classes = ["has-plugin-style"];
    const knownPlugins = ["control", "logic", "time", "data", "telegram", "twilio", "mqtt"];
    if (knownPlugins.includes(plugin)) classes.push(plugin);
    else classes.push("default");
    return classes;
  },
};

const actions = {
  restore({ commit, dispatch }) {
    // 1. Reset Zoom Level
    const zoom = getCrossSessionItem("zoom") || 1;
    commit("SET_ZOOM", zoom);
    // 2. check if login screen should be displayed
    dispatch("tryAuth", {});
  },
  reset({ dispatch }) {
    dispatch("live/setIsLive", false, { root: true });
    dispatch("game/clear", null, { root: true });
    dispatch("collections/clear", null, { root: true });
  },
  initView({ rootState, dispatch, commit }, { view, game, level, query }) {
    // set Game and Level store values based on router parameter object
    if (game && game != rootState.game.selected) dispatch("game/select", game, { root: true });
    if (game && level && level != rootState.level.selected) dispatch("level/select", { game, level }, { root: true });
    if (game && !level && rootState.level.selected !== "") dispatch("level/clearLevel", undefined, { root: true });
    // set live mode and select session based on router query object
    if (hasKey(query, "mode")) dispatch("live/setIsLive", query.mode == "live", { root: true });
    if (hasKey(query, "session")) dispatch("live/selectSession", query.session, { root: true });
    // set new view to potentially trigger socket namespace changes
    commit("CHANGE_VIEW", view);
  },
  getGames({ commit }) {
    this.$api.getGames().then((res) => {
      commit("SET_GAMES", res.data);
    });
  },
  createGame({ dispatch }, options) {
    // options = { name: options.name, template: "basic" }
    if (isUrlSafe(options.name)) {
      return this.$api.createGame(options).then(() => dispatch("getGames"));
    } else {
      const name = options.name;
      const message = i18n("ERROR.CHARACTERS", { target: "game", name });
      dispatch("adaptor/addLog", { type: "error", message }, { root: true });
      return Promise.reject(new Error("forbbiden characters in name: " + name));
    }
  },
  deleteGame({ dispatch }, game) {
    this.$api.deleteGame({ game }).then(() => dispatch("getGames"));
  },
  tryAuth({ dispatch }, { username, password }) {
    const token = setAuthToken(username, password);
    return this.$api
      .checkCredentials(token)
      .then((authorized) => {
        if (username === undefined && token) username = atob(token).split(":")[0];
        dispatch("setUser", username);
        dispatch("setAuth", authorized);
        return authorized;
      })
      .catch((error) => {
        dispatch("addLog", { type: "error", message: error });
        return false;
      });
  },
  setUser({ commit }, user) {
    if (user) commit("SET_USER", user);
  },
  setAuth({ commit, dispatch }, authorized) {
    if (!authorized) {
      dispatch("setUser", undefined);
      removeCrossSessionItem("authtoken");
    }
    commit("SET_AUTH", authorized);
  },
  addOtherUser({ commit }, user) {
    const index = state.others.findIndex((other) => other.socket == user.socket);
    if (index < 0) commit("ADD_OTHER_USER", user);
  },
  removeOtherUser({ commit }, user) {
    const index = state.others.findIndex((other) => other.socket == user.socket);
    if (index >= 0) commit("REMOVE_OTHER_USER", user);
  },
  clearOtherUsers({ commit }) {
    commit("SET_OTHER_USERS", []);
  },
  addLog({ commit }, { type = "info", message }) {
    // ToDo: filter for config logLevel
    switch (type) {
      case "info":
        toast.info(message);
        break;
      case "success":
        toast.success(message);
        break;
      case "error":
        toast.error(message);
        break;
      case "warn":
      case "warning":
        toast.warning(message);
        break;
      default:
        toast(message);
        break;
    }
    commit("ADD_LOG", { type, message });
  },
  // Note: Not used right now, because we fit the stage viewport to level States and not last zoom level.
  // setZoom({ commit }, zoom) {
  //   commit("SET_ZOOM", zoom);
  //   setCrossSessionItem("zoom", zoom);
  // },
  triggerPrompt({ commit }, promptData) {
    commit("SET_PROMPT", Object.assign(promptData, { visible: true }));
  },
  cancelPrompt({ commit }) {
    commit("SET_PROMPT", {
      message: null,
      webhook: null,
      schema: null,
      visible: false,
    });
  },
  async answerPrompt({ dispatch, state }, answer) {
    // ToDo: add support for different prompt methods [post, get]
    await this.$api.post(state.prompt.webhook, answer).catch(console.error);
    dispatch("cancelPrompt");
  },
};

const mutations = {
  CHANGE_VIEW(state, view) {
    state.view = view;
  },
  SET_USER(state, username) {
    state.username = username;
  },
  SET_AUTH(state, authorized) {
    state.authorized = authorized;
  },
  ADD_OTHER_USER(state, username) {
    state.others.push(username);
  },
  REMOVE_OTHER_USER(state, index) {
    state.others.splice(index, 1);
  },
  SET_OTHER_USERS(state, others) {
    state.others = others;
  },
  SET_GAMES(state, games) {
    state.games = games;
  },
  ADD_LOG(state, log) {
    state.clientLog.push(log);
  },
  SET_ZOOM(state, zoom) {
    state.zoom = zoom;
  },
  SET_PROMPT(state, prompt) {
    state.prompt = prompt;
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};

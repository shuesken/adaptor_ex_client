const state = {
  level: "info", // one of "error", "warn", "info", "debug", "trace")
  isOpen: false, // is console open
  messages: [],
};

const getters = {};

const actions = {
  toggle({ state, dispatch, commit }) {
    if (state.isOpen) {
      commit("TOGGLE", false);
      dispatch("clearMessages");
    } else commit("TOGGLE", true);
  },
  changeLogLevel({ commit, dispatch }, level) {
    dispatch("clearMessages").then(commit("CHANGE_LOG_LEVEL", level));
  },
  addMessages({ commit }, messages) {
    commit("ADD_MESSAGES", { messages, maximum: 50 });
  },
  clearMessages({ commit }) {
    commit("CLEAR_MESSAGES");
  },
};

const mutations = {
  TOGGLE(state, open) {
    state.isOpen = open;
  },
  CHANGE_LOG_LEVEL(state, level) {
    state.level = level;
  },
  ADD_MESSAGES(state, { messages, maximum }) {
    const combined = state.messages.concat(messages);
    if (combined.length > maximum) combined.splice(0, combined.length - maximum);
    state.messages = combined;
  },
  CLEAR_MESSAGES(state) {
    state.messages = [];
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};

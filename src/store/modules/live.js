import { isUrlSafe, i18n, createId } from "@utils";
import { cloneDeep } from "lodash";

const state = {
  sessions: [],
  session: "", // _id of selected session
  items: [], // list of entities available in dashboard stream
  isLive: false,
};

const getters = {
  sessionsByLevel: (state) => {
    return state.sessions.reduce((result, currentValue) => {
      if (!result[currentValue["level"]]) result[currentValue["level"]] = [];
      result[currentValue["level"]].push(currentValue);
      return result;
    }, {});
  },
  sessionsOfLevel: (state) => (level) => {
    return state.sessions.filter((session) => session.level == level);
  },
  currentLiveStates: (state) => (sessionId) => {
    if (!sessionId) sessionId = state.session;
    const session = state.sessions?.find((s) => s._id == sessionId);
    return session ? session.current_states.map((c) => c.id) : [];
  },
};

const actions = {
  toggle({ state, dispatch }) {
    dispatch("setIsLive", !state.isLive);
  },
  setIsLive({ rootState, commit, dispatch }, isLive) {
    if (isLive) {
      dispatch("getSessions", rootState.game.selected);
      dispatch("level/unselectAll", undefined, { root: true });
    } else {
      dispatch("selectSession", "");
    }
    commit("SET_IS_LIVE", isLive);
  },
  selectSession({ commit }, session) {
    commit("SELECT_SESSION", session);
  },
  launchSession({ dispatch }, { game, level, session, args }) {
    if (!session) session = "session_" + createId(8);
    else if (!isUrlSafe(session)) {
      const message = i18n("ERROR.CHARACTERS", {
        target: "Session name",
        name: session,
      });
      dispatch("adaptor/addLog", { type: "error", message }, { root: true });
      return;
    }
    return this.$api
      .launchSession({ game }, { level, name: session, arguments: args })
      .then((result) => {
        const message = i18n("SESSION.LAUNCH", { level, session });
        dispatch("adaptor/addLog", { type: "info", message }, { root: true });
        return result;
      })
      .catch((error) => {
        const message = i18n("SESSION.LAUNCH_ERROR", {
          session,
          level,
          error: error.toString(),
        });
        dispatch("adaptor/addLog", { type: "error", message }, { root: true });
        return;
      });
  },
  cancelSession({ rootState, state, dispatch, commit }, { game, session }) {
    if (!game) game = rootState.game.selected;
    if (state.session == session._id) commit("UNSELECT_SESSION");
    this.$api
      .cancelSession({ game, session: session._id })
      .then(() => {
        const message = i18n("SESSION.CANCEL", { game, session: session.name });
        dispatch("adaptor/addLog", { type: "warn", message }, { root: true });
      })
      .catch(console.error);
  },
  getSessions({ commit }, game) {
    return this.$api.findSessions({ game }).then((result) => {
      commit("SET_SESSIONS", result.data);
    });
  },
  updateSessions({ commit }, sessions) {
    const data = cloneDeep(state.sessions);
    sessions.forEach((session) => {
      let index = data.findIndex((s) => s?._id === session._id);
      index >= 0 ? (data[index] = session) : data.push(session);
    });
    commit("SET_SESSIONS", data);
  },
  removeSessions({ commit }, sessions) {
    const data = cloneDeep(state.sessions);
    sessions.forEach((session) => {
      let index = data.findIndex((s) => s?._id === session);
      data.splice(index, index >= 0 ? 1 : 0);
    });
    commit("SET_SESSIONS", data);
  },
  triggerState({ rootState, state }, stateId) {
    const game = rootState.game.selected;
    const session = state.session;
    // NOTE: should we default to use socket connection if there is one to trigger next!?
    this.$api.next({ game, session }, { id: stateId }).catch(console.error);
  },
  getItems({ rootState, commit }, { game, query }) {
    if (!game) game = rootState.game.selected;
    this.$api
      .findItems({ game, ...query })
      .then((res) => {
        const items = cloneDeep(state.items);
        for (let n = res.data.length - 1; n >= 0; n--) {
          const item = res.data[n];
          const index = items.findIndex((i) => i?._id === item._id);
          if (index >= 0) {
            items.splice(index, 1);
          }
          items.unshift(item);
        }
        commit("SET_ITEMS", items);
      })
      .catch(console.error);
  },
  updateItems({ commit }, items) {
    const data = cloneDeep(state.items);
    items.forEach((item) => {
      let index = data.findIndex((s) => s?._id === item._id);
      index >= 0 ? (data[index] = item) : data.push(item);
    });
    commit("SET_ITEMS", data);
  },
  removeItems({ commit }, items) {
    const data = cloneDeep(state.items);
    items.forEach((item) => {
      let index = data.findIndex((s) => s?._id === item._id);
      data.splice(index, index >= 0 ? 1 : 0);
    });
    commit("SET_ITEMS", data);
  },
};

const mutations = {
  SET_IS_LIVE(state, open) {
    state.isLive = open;
  },
  SELECT_SESSION(state, session) {
    state.session = session;
  },
  UNSELECT_SESSION(state) {
    state.session = "";
  },
  SET_SESSIONS(state, sessions) {
    state.sessions = sessions;
  },
  SET_ITEMS(state, items) {
    state.items = items;
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};

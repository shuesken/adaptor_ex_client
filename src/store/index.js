import { createStore } from "vuex";

import createSocketsPlugin from "@/store/plugins/sockets";
import createDebugLogPlugin from "@/store/plugins/debug";

import adaptor from "@/store/modules/adaptor";
import game from "@/store/modules/game";
import plugins from "@/store/modules/plugins";
import collections from "@/store/modules/collections";
import level from "@/store/modules/level";
import states from "@/store/modules/states";
import serverLog from "@/store/modules/serverLog";
import live from "@/store/modules/live";

export default (config, socketManager) =>
  createStore({
    strict: true,
    modules: {
      adaptor,
      game,
      collections,
      plugins,
      level,
      states,
      serverLog,
      live,
    },
    plugins: [
      ...(config?.LOG_STORE === true ? [createDebugLogPlugin()] : []),
      createSocketsPlugin(socketManager, config?.LOG_SOCKET === true),
    ],
  });

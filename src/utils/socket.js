// This is the socket base class. It is used in the sockets store plugin.

export default class Socket {
  constructor(manager, store, namespace, listener, debugLog = false) {
    this.manager = manager;
    this.socket = undefined;
    this.store = store;
    this.nsp = namespace;
    this.listener = this.assignListener(listener);
    this.options = { auth: { token: localStorage.authtoken } };
    this.debugLog = debugLog;
  }

  open(...params) {
    if (this.socket) {
      if (this.nsp(...params) == this.socket?.nsp) {
        if (this.socket.connected) {
          // console.log(this.nsp(...params), "socket already connected");
          return;
        }
      } else {
        // console.log(this.nsp(...params), "socket exists but different nsp");
        this.close();
      }
    }
    // console.log(this.nsp(...params), "connect socket");
    this.socket = this.manager.socket(this.nsp(...params), this.options);
    this.addListeners();
    this.socket.connect();
    if (this.debugLog) {
      console.log(this.socket);
      this.socket.onAny((m, data) => {
        console.log("SOCKET", m, data);
      });
    }
  }

  close() {
    if (!this.socket) return;
    // console.log("close()", this.socket.nsp, this.socket);
    this.socket.disconnect();
    this.socket.removeAllListeners();
  }

  emit(message, data) {
    if (!this.socket) return;
    this.socket.emit(message, data);
  }

  addListeners() {
    Object.entries(this.listener).forEach(([eventName, callback]) =>
      this.socket.on(eventName, (data) => callback(data, this.store))
    );
  }

  assignListener(listener) {
    return Object.assign(
      {
        connect: () => {}, //console.log("Socket " + this.socket.nsp, "connected"),
        reconnect: () => console.log("Socket " + this.socket.nsp, "reconnected"),
        connect_error: (e, store) => {
          store.dispatch("adaptor/addLog", {
            type: "error",
            message: `Socket Error: ${e}`,
          });
          if (this.debugLog) console.log("Socket " + this.socket.nsp, e);
        },
        disconnect: (reason) => {
          if (this.debugLog) console.log("Socket " + this.socket.nsp, "disconnected, because: ", reason);
        },
      },
      listener
    );
  }

  isConnected() {
    return this.socket && this.socket.connected;
  }
}
